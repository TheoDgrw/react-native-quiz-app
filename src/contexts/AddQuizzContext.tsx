import React, { useContext } from "react";
import { FC } from "react";
import IQuiz, { IAnswer, IQuestion } from "../interfaces/IQuiz";
import MainContext from "./Context";

interface ProviderValue {
    quizzName: string,
    onChangeQuizzName: (text: string) => void,
    questions: IQuestion[],
    onChangeQuestion: (text: IQuestion[]) => void,
    quizz : IQuiz
    onChangeQuizz: (text: IQuiz) => void

}
const AddQuizzContext = React.createContext({} as ProviderValue);

export const AddQuizzProvider: FC = ({children}) => {  
    const [quizzName, onChangeQuizzName ] = React.useState("");
    const [questions, onChangeQuestion ] = React.useState<IQuestion[]>([]);
    const { text } = useContext(MainContext)
    const [quizz, onChangeQuizz ] = React.useState<IQuiz>({author: text, questions: questions})


    return (
        <AddQuizzContext.Provider value={{ quizzName, onChangeQuestion, questions, onChangeQuizzName, quizz, onChangeQuizz }} >
            {children}
        </AddQuizzContext.Provider>
    )
}

export default AddQuizzContext;
