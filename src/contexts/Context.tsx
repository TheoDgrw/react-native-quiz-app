import React from "react";

interface ProviderValue {
    onChangeText: (text: string) => void,
    text: string
}
const MainContext = React.createContext({} as ProviderValue);

export default MainContext;