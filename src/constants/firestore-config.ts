import { API_KEY, APP_ID, PROJECT_ID, AUTH_DOMAIN, STORAGE_BUCKET, DATABASE_URL, MESSAGING_SENDER_ID } from "@env";

export const config = {
    apiKey: API_KEY,
    appId: APP_ID,
    projectId: PROJECT_ID,
    authDomain: AUTH_DOMAIN,
    databaseURL: DATABASE_URL,
    storageBucket: STORAGE_BUCKET,
    messagingSenderId: MESSAGING_SENDER_ID,
};