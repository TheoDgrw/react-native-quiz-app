export interface IAnswer {
    id: number
    content: string;
    isCorrect: boolean;
}

export interface IQuestion {
    question: string;
    answers: IAnswer[];
}

export default interface IQuiz {
    author: string;
    questions: IQuestion[];
}