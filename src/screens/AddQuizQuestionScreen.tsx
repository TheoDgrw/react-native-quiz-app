import { useNavigation } from "@react-navigation/native";
import React from "react";
import { useContext } from "react";
import { Button, ScrollView } from "react-native";
import { StyleSheet, Text, TextInput, View } from "react-native";
import AddQuizzContext from "../contexts/AddQuizzContext";
import { addQuiz } from "../hooks/firestore/functions";
import useAddQuizz from "../hooks/useAddQuizz";
import { screens } from "./enum";

const AddQuizQuestionScreen = () => {
  const navigation = useNavigation();
  const {author, questions,quizzName,setQuestion,question,answers,setAnswers,submitQuestion, quizz, onChangeQuizz} = useAddQuizz()
  return (
    <ScrollView>
      <View style={styles.container}>
        <Text>Quizz {quizzName} </Text>
        <Text>Question {questions.length +1 } / 5</Text>
        <TextInput 
                  style={styles.input}
                  onChangeText={input=> {setQuestion(input)}}
                  value={question}
                  placeholder="Question"
              />
        <TextInput 
                  style={styles.input}
                  onChangeText={input => {setAnswers([...answers.filter((answer) => answer.id !== 1 ), {content: input, id: 1, isCorrect: true}])}}
                  value={answers.find(answer=> answer.id === 1)?.content}
                  placeholder="Réponse 1"
              />
        <TextInput 
                  style={styles.input}
                  onChangeText={input => {setAnswers([...answers.filter((answer) => answer.id !== 2 ), {content: input, id: 2, isCorrect: false}])}}
                  value={answers.find(answer=> answer.id === 2)?.content}
                  placeholder="Réponse 2"
              />
        <TextInput 
                  style={styles.input}
                  onChangeText={input => {setAnswers([...answers.filter((answer) => answer.id !== 3 ), {content: input, id: 3, isCorrect: false}])}}
                  value={answers.find(answer=> answer.id === 3)?.content}
                  placeholder="Réponse 3"
              />
        <TextInput 
                  style={styles.input}
                  onChangeText={input => {setAnswers([...answers.filter((answer) => answer.id !== 4 ), {content: input, id: 4, isCorrect: false}])}}
                  value={answers.find(answer=> answer.id === 4)?.content}
                  placeholder="Réponse 4"
              />
              <Button 
              title="Valider question"
              disabled={questions.length < 5 ? false : true}
              onPress={() => {submitQuestion();
                              if(questions.length < 4) {navigation.navigate(screens.ADD_QUIZ_QUESTION)} 
                              else{ onChangeQuizz({author: author, questions: questions})
                                    addQuiz(quizzName, quizz) }}}
              />
      </View>
    </ScrollView>
    
  );
};

export default AddQuizQuestionScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    height: 40,
    width: 120,
    margin: 12,
    borderWidth: 1,
    backgroundColor: "#ffffff",
    paddingHorizontal: 5
  },
});
