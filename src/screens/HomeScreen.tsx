import { useNavigation } from "@react-navigation/native";
import React, { useContext } from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import MainContext from "../contexts/Context";
import { stacks } from "../navigation/enum";
import { screens } from "./enum";

const HomeScreen = () => {
  const navigation = useNavigation();
  const { text } = useContext(MainContext)
  return (
    <View style={styles.container}>
      <Text>Bonjour {text} </Text>
      <Button 
        title="Créer un Quizz"
        onPress={() => { navigation.navigate(stacks.ADD_QUIZ) }}
      />
      <Button 
        title="Tenter un Quizz"
        onPress={() => { navigation.navigate(screens.QUIZ_LIST) }}
      />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
