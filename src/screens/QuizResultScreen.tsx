import { RouteProp, useRoute } from "@react-navigation/native";
import React from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import Result from "../components/Result";
import { colors } from "../constants/colors";
import { QuizStackParamsList } from "../navigation/QuizStack";
import { screens } from "./enum";

const QuizResultScreen = () => {
  const { params } = useRoute<RouteProp<QuizStackParamsList, screens.QUIZ_RESULTS>>();
  const { quiz, responses } = params;

  const score = quiz.questions.reduce((acc, { answers }, index) => {
    const validRes = answers.find((answer) => {
      return answer.isCorrect === true && responses[index].content === answer.content;
    });
    return validRes !== undefined ? acc + 1 : acc;
  }, 0);

  return (
    <View style={styles.container}>
      <Text style={styles.score}>Score : {score}</Text>
      <FlatList
        data={quiz.questions}
        renderItem={({ item, index }) => <Result
          question={item.question}
          answers={item.answers}
          answer={responses[index]}
        />}
      />
    </View>
  );
};

export default QuizResultScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.blue
  },
  score: {
    color: '#FFF',
    fontSize: 30,
    marginBottom: 10,
  },
});