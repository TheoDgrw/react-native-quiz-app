import { useNavigation } from "@react-navigation/native";
import React from "react";
import { useContext } from "react";
import { Button, StyleSheet, Text, TextInput, View } from "react-native";
import AddQuizzContext from "../contexts/AddQuizzContext";
import { screens } from "./enum";

const AddQuizNameScreen = () => {
    const navigation = useNavigation();
    const {onChangeQuizzName, quizzName } = useContext(AddQuizzContext)
    return (
        <View style={styles.container}>
            <Text>Hello AddQuizName !</Text>
            <TextInput 
                style={styles.input}
                onChangeText={onChangeQuizzName}
                value={quizzName}
            />
            <Button
                title="Valider Titre"
                onPress={() => { navigation.navigate(screens.ADD_QUIZ_QUESTION) }} />
        </View>
    );
};

export default AddQuizNameScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    input: {
        height: 40,
        width: 120,
        margin: 12,
        borderWidth: 1,
        backgroundColor: "#ffffff",
        paddingHorizontal: 5
      },
});
