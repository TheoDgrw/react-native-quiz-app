import React from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import Quiz from "../components/Quiz";
import Splash from "../components/Splash";
import { getAllQuiz } from "../hooks/firestore/functions";
import useApiCB from "../hooks/firestore/useApiCB";
import IQuiz from "../interfaces/IQuiz";

const QuizListScreen = () => {
  const { isLoading, res, error } = useApiCB(getAllQuiz);

  if (isLoading) return <Splash />;

  if (error || !res) return <Text>Une erreur est survenue</Text>;

  const datas = res.docs.map((doc) => {
    return { data: doc.data(), name: doc.id }
  });

  // const datas = [
  //   {
  //     name: 'premier',
  //     data: {
  //       author: 'test',
  //       questions: [
  //         {
  //           question: 'Qui suis-je ?',
  //           answers: [
  //             {
  //               content: 'Théo',
  //               isCorrect: false
  //             },
  //             {
  //               content: 'Bryan',
  //               isCorrect: false
  //             },
  //             {
  //               content: 'Le Boss',
  //               isCorrect: true
  //             },
  //             {
  //               content: 'Une chaise',
  //               isCorrect: false
  //             }
  //           ]
  //         },
  //         {
  //           question: 'Quel est mon meuble préféré ?',
  //           answers: [
  //             {
  //               content: 'Un placard',
  //               isCorrect: true
  //             },
  //             {
  //               content: 'Une boussole',
  //               isCorrect: false
  //             },
  //             {
  //               content: 'Un savon',
  //               isCorrect: true
  //             },
  //             {
  //               content: 'Une chaise',
  //               isCorrect: false
  //             }
  //           ]
  //         }
  //       ]
  //     }
  //   },
  //   {
  //     name: 'second',
  //     data: {
  //       author: 'test',
  //       questions: [
  //         {
  //           question: 'Qui suis-je ?',
  //           answers: [
  //             {
  //               content: 'Théo',
  //               isCorrect: false
  //             },
  //             {
  //               content: 'Bryan',
  //               isCorrect: false
  //             },
  //             {
  //               content: 'Le Boss',
  //               isCorrect: true
  //             },
  //             {
  //               content: 'Une chaise',
  //               isCorrect: false
  //             }
  //           ]
  //         },
  //         {
  //           question: 'Quel est mon meuble préféré ?',
  //           answers: [
  //             {
  //               content: 'Un placard',
  //               isCorrect: true
  //             },
  //             {
  //               content: 'Une boussole',
  //               isCorrect: false
  //             },
  //             {
  //               content: 'Un savon',
  //               isCorrect: true
  //             },
  //             {
  //               content: 'Une chaise',
  //               isCorrect: false
  //             }
  //           ]
  //         }
  //       ]
  //     }
  //   }
  // ];

  return (
    <View style={styles.container}>
      <FlatList
        data={datas}
        renderItem={({ item }) => {
          return <Quiz
            quiz={item.data as IQuiz}
            quizName={item.name}
          />;
        }}
      />
    </View>
  );
};

export default QuizListScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: '#1e81b0'
  },
});
