import { useNavigation } from "@react-navigation/native";
import React from "react";
import { useContext } from "react";
import { Button, Platform, SafeAreaView, StyleSheet, Text, TextInput, View } from "react-native";
import MainContext from "../contexts/Context";
import { stacks } from "../navigation/enum";

const AuthScreen = () => {
  const navigation = useNavigation();
  const {onChangeText, text} = useContext(MainContext);
  return (
    <SafeAreaView style={styles.container}>

      <Text style={[styles.title, styles.text]}>Quizz</Text>
      <View>
        <Text style={[styles.text]}>Veuillez renseigner votre nom</Text>
        <TextInput
        style={styles.input}
        onChangeText={onChangeText}
        value={text}
        />
        <Button
        title="Confirmer"
        onPress={() => {navigation.navigate(stacks.HOME)}}
      />
      </View>
    </SafeAreaView>
  );
};

export default AuthScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1e81b0",
    paddingTop: Platform.OS === 'android' ? 25 : 0,
    alignItems: "center",
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    backgroundColor: "#ffffff",
    paddingHorizontal: 5
  },
  text: {
    color: "#ffffff",
  },
  title: {
    fontSize: 40
  }
});
