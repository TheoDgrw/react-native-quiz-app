import React, { useState } from "react";
import { screens } from "./enum";
import { QuizStackParamsList } from "../navigation/QuizStack";
import { RouteProp, useNavigation, useRoute } from "@react-navigation/native";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { IAnswer } from "../interfaces/IQuiz";
import { colors } from "../constants/colors";
import CstmTouchableHighlight from '../components/CstmTouchableHighlight';
import { SafeAreaView } from "react-native-safe-area-context";
import Before from "../components/AnswersFlatList/Before";
import Below from "../components/AnswersFlatList/Below";

export interface IState {
  currIndex: number;
  responses: IAnswer[]
}

const QuizQuestionScreen = () => {
  const [state, setState] = useState<IState>({ currIndex: 0, responses: []});
  const route = useRoute<RouteProp<QuizStackParamsList, screens.QUIZ_QUESTION>>();
  const { quiz, quizName } = route.params;
  const currQuestion = quiz.questions[state.currIndex] ? quiz.questions[state.currIndex] : null;

  const { navigate } = useNavigation();

  const handlePress = (answer: IAnswer) => {
    const isAdd = state.responses[state.currIndex] ? false : true;
    if (isAdd) {
      setState({
        currIndex: state.currIndex,
        responses: [...state.responses, answer]
      });
    } else {
      const newResponses = [...state.responses];
      newResponses[state.currIndex] = answer;
      setState({
        currIndex: state.currIndex,
        responses: newResponses
      });
    }
  }

  const sendQuiz = () => {
    navigate(screens.QUIZ_RESULTS, { responses: state.responses, quiz });
  }

  if (currQuestion === null) {
    return <Text style={{ color: 'white' }}>PAS DE QUESTIONS !!</Text>
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={{alignSelf: 'stretch', alignItems: 'center'}}>
        <FlatList
          style={{width: '100%', display: 'flex'}}
          contentContainerStyle={{ alignItems: 'center', alignSelf: 'stretch' }}
          ListHeaderComponent={<Before quizName={quizName} currQuestion={currQuestion.question} />}
          ListFooterComponent={<Below state={state} setState={setState} quiz={quiz} sendQuiz={sendQuiz} />}
          nestedScrollEnabled
          keyExtractor={({ content }, i) => i.toString()}
          data={currQuestion.answers}
          renderItem={({ item }) => {
            return <CstmTouchableHighlight state={state} content={item.content} onPress={() => handlePress(item)}>
              <Text style={[styles.answerText]}>{item.content}</Text>
            </CstmTouchableHighlight>
          }}
        />
      </View>
    </SafeAreaView>
  );
};

export default QuizQuestionScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: "center",
    // justifyContent: "center",
    backgroundColor: colors.blue,
    color: '#FFF',
    width: '100%',
  },
  scrollViewContainer: {
    alignItems: 'center',
    flex: 1,
  },
  scrollView: {
    width: '100%',
  },
  title: {
    fontSize: 40,
    // width: '50%',
    color: colors.orange,
    borderBottomWidth: 5,
    borderColor: colors.orange,
    marginBottom: 30,
    textShadowColor: '#FFF',
    shadowOpacity: 0.5,
    textShadowOffset: {
      height: 0.5,
      width: -0.5,
    },
    textShadowRadius: 2,
  },
  whiteText: {
    color: '#FFF',
    textAlign: 'center',
  },
  quizContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 150
  },
  answerText: {
    color: 'black',
    textAlign: 'center',
  },
  btn: {
    backgroundColor: 'red',
  }
});
