import React from "react";
import { useNavigation } from "@react-navigation/native";
import { Pressable, StyleSheet, Text, View } from "react-native";
import IQuiz from "../interfaces/IQuiz";
import { screens } from "../screens/enum";
import GenericTouchableHighlight from "./GenericTouchableHighlight";

interface IProps {
    quiz: IQuiz;
    quizName: string
}

export default function Quiz({ quiz, quizName }: IProps) {
    const { navigate } = useNavigation();

    return <View style={styles.container}>
        <GenericTouchableHighlight
            onPress={() => navigate(screens.QUIZ_QUESTION, { quiz, quizName })}
        >
            <View style={styles.view}>
                <Text style={styles.quizName}>{quizName}</Text>
                <Text style={styles.author}>{quiz.author}</Text>
            </View>
        </GenericTouchableHighlight>
    </View>;
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 30
    },
    view: {
        flexDirection: 'row',
    },
    quizName: {
        width: '50%',
    },
    author: {
        width: '50%',
        textAlign: 'right'
    }
});