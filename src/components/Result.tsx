import React from "react";
import { StyleSheet } from "react-native";
import { View } from "react-native";
import { Text, FlatList } from "react-native";
import { colors } from "../constants/colors";
import { IAnswer, IQuestion } from "../interfaces/IQuiz";
import { Entypo, AntDesign } from '@expo/vector-icons'

interface IProps {
    question: string;
    answers: IAnswer[];
    answer: IAnswer;
}

export default function Result({ question, answers, answer }: IProps) {

    const findStyle = (quizAnswer: IAnswer) => {
        return quizAnswer.isCorrect ?
            styles.correct
            : styles.default;
    }

    const findIcon = (quizAnswer: IAnswer) => {
        if (quizAnswer.content === answer.content) {

            return answer.isCorrect ?
                <AntDesign name="checkcircle" style={styles.icon} size={22} color="green" />
                : <Entypo name="circle-with-cross" style={styles.icon} size={24} color="red" />;
        }

        return null;
    };

    return <View style={styles.container}>
        <Text style={styles.question}>{question}</Text>
        <FlatList
            data={answers}
            renderItem={({ item }) => {
                return <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={[findStyle(item), styles.allText]}>
                        { item.content }
                    </Text>
                    {findIcon(item)}
                </View>
            }}
        />
    </View>
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: '#FFF',
        marginBottom: 20,
        alignItems: 'center',
    },
    question: {
        color: colors.orange,
        fontSize: 18,
        paddingHorizontal: 30,
        textAlign: 'center',
        marginBottom: 10,
    },
    correct: {
        color: 'green',
    },
    default: {
        color: 'black',
    },
    allText: {
        textAlign: 'center',
        borderColor: colors.blue,
        borderWidth: 3,
        borderBottomWidth: 0,
        width: 300,
        paddingVertical: 10,
    },
    icon: {
        left: 10
    }
});