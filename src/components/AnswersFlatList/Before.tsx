import React from "react";
import { StyleSheet } from "react-native";
import { Text } from "react-native";
import { colors } from "../../constants/colors";

interface IProps {
    currQuestion: string;
    quizName: string;
}

export default function Before({ currQuestion, quizName }: IProps) {

    return <>
        <Text style={styles.title}>{quizName}</Text>
        <Text style={styles.question}>{currQuestion}</Text>
    </>;
}

const styles = StyleSheet.create({
    question: {
        fontSize: 20,
        backgroundColor: '#FFF',
        alignSelf: 'stretch',
        color: colors.orange,
        textAlign: 'center',
        borderTopWidth: 4,
        borderColor: colors.orange,
        marginBottom: 10,
    },
    title: {
        fontSize: 40,
        // width: '50%',
        color: colors.orange,
        // borderBottomWidth: 5,
        // borderColor: colors.orange,
        marginBottom: 30,
        textShadowColor: '#FFF',
        shadowOpacity: 0.5,
        textShadowOffset: {
            height: 0.5,
            width: -0.5,
        },
        textShadowRadius: 2,
    },
});