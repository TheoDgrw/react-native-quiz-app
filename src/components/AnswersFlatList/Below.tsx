import React, { Dispatch, SetStateAction} from "react";
import IQuiz from "../../interfaces/IQuiz";
import { IState } from "../../screens/QuizQuestionScreen";
import { TouchableHighlight, Text, Button, NativeSyntheticEvent, NativeTouchEvent, StyleSheet } from "react-native";
import { colors } from "../../constants/colors";
import { useState } from "react";
import { View } from "react-native";

interface IProps {
    state: IState;
    setState: Dispatch<SetStateAction<IState>>;
    quiz: IQuiz;
    sendQuiz: () => void;
}


/**
 * 
 * @param param0 
 * @returns 
 */
export default function Below({ state, setState, quiz, sendQuiz }: IProps) {
    // TODO: reafctor state and quiz props to use a context provider instead
    // TODO: use a component for TouchableOpacity, common isPress is bad, change it
    const [isPress, setIsPress] = useState(false);

    const props = {
        underlayColor: colors.orange,
        onHideUnderlay: () => setIsPress(false),
        onShowUnderlay: () => setIsPress(true),
        style: isPress ? [styles.btn, styles.btnPress] : [styles.btn, styles.btnNormal]
    }

    return <View style={{ flexDirection: 'row' }}>
        {state.currIndex > 0 ?
            <TouchableHighlight
                {...props}
                onPress={() => setState({...state, currIndex: state.currIndex - 1})}
            >
                <Text style={styles.text}>Précédent</Text>
            </TouchableHighlight>
            : null
        }
        {state.currIndex === quiz.questions.length - 1 ?
            <TouchableHighlight
                {...props}
                onPress={() => sendQuiz()}
            >
                <Text style={styles.text}>Envoyer</Text>
            </TouchableHighlight>
            : <TouchableHighlight
                {...props}
                onPress={() => setState({...state, currIndex: state.currIndex + 1})}
            >
                <Text style={styles.text}>Suivant</Text>
            </TouchableHighlight>
        }
    </View>;
}

const styles = StyleSheet.create({
    btn: {
        backgroundColor: colors.orange,
        borderWidth: 4,
        borderRadius: 10,
        marginVertical: 25,
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: 150,
    },
    btnPress: {
        borderColor: colors.orange
    },
    btnNormal: {
        borderColor: '#FFF',
    },
    text: {
        color: '#FFF',
        textAlign: 'center'
    }
});