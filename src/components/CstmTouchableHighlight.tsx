import React, { PropsWithChildren, useState } from "react";
import { useEffect } from "react";
import { ReactChildren } from "react";
import { StyleSheet } from "react-native";
import { GestureResponderEvent, StyleProp, ViewStyle } from "react-native";
import { TouchableHighlight } from "react-native-gesture-handler";
import { colors } from "../constants/colors";
import { IState } from "../screens/QuizQuestionScreen";

interface IProps {
    onPress: (((event: GestureResponderEvent) => void) & (() => void)) | undefined,
    state: IState,
    content: string,
}

export default function CstmTouchableHighlight({ state, content, onPress, children }: PropsWithChildren<IProps>) {
    const [isPress, setIsPress] = useState(state.responses[state.currIndex] ? state.responses[state.currIndex].content === content : false);

    const testRes = state.responses[state.currIndex] ? state.responses[state.currIndex].content === content : false;

    useEffect(() => {
        setIsPress(testRes);
    }, [state.responses[state.currIndex]]);

    return <TouchableHighlight
        onPress={onPress}
        underlayColor="#F75C1E"
        style={isPress ? [styles.default, styles.btnPress] : [styles.default, styles.btnNormal]}
        onHideUnderlay={() => setIsPress(testRes)}
        onShowUnderlay={() => setIsPress(true)}
    >
        {children}
    </TouchableHighlight>;
}

const styles = StyleSheet.create({
    default: {
        marginTop: 10,
        borderRadius: 20,
        paddingVertical: 20,
        paddingHorizontal: 40,
        borderWidth: 4,
        width: 300,
        backgroundColor: colors.orange,
    },
    btnPress: {
        borderColor: 'white',
        color: '#FFF'
    },
    btnNormal: {
        borderColor: '#F75C1E',
        backgroundColor: 'white',
    }
});