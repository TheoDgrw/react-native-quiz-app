import React, { PropsWithChildren, useState } from "react";
import { StyleSheet } from "react-native";
import { GestureResponderEvent, StyleProp, ViewStyle } from "react-native";
import { TouchableHighlight } from "react-native-gesture-handler";
import { IState } from "../screens/QuizQuestionScreen";

interface IProps {
    onPress: (((event: GestureResponderEvent) => void) & (() => void)) | undefined,
}

export default function GenericTouchableHighlight({ onPress, children }: PropsWithChildren<IProps>) {
    const [isPress, setIsPress] = useState(false);

    return <TouchableHighlight
        onPress={onPress}
        underlayColor="#F75C1E"
        style={isPress ? [styles.default, styles.btnPress] : [styles.default, styles.btnNormal]}
        onHideUnderlay={() => setIsPress(false)}
        onShowUnderlay={() => setIsPress(true)}
    >
        {children}
    </TouchableHighlight>;
}

const styles = StyleSheet.create({
    default: {
        marginTop: 10,
        borderRadius: 20,
        paddingVertical: 20,
        paddingHorizontal: 40,
        borderWidth: 4,
        width: 300,
    },
    btnPress: {
        borderColor: 'white',
        color: '#FFF'
    },
    btnNormal: {
        borderColor: '#F75C1E',
        backgroundColor: 'white',
    }
});