import React from "react";
import { ActivityIndicator, View } from "react-native";

export default function Splash() {

    return <View>
        <ActivityIndicator size="large" color="blue" />
    </View>
}