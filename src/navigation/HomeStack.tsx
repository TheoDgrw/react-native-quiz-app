import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { routes } from "../constants/routes";
import { screens } from "../screens/enum";
import HomeScreen from "../screens/HomeScreen";
import AddQuizStack from "./AddQuizStack";
import { stacks } from "./enum";
import QuizStack from "./QuizStack";

export type HomeStackParamsList = {
    [screens.HOME]: undefined;
    [screens.QUIZ_LIST]: undefined;
    [stacks.ADD_QUIZ]: undefined;
}

const { Navigator, Screen } = createStackNavigator<HomeStackParamsList>();

const HomeStack = () => (
    <Navigator initialRouteName={screens.HOME} headerMode="none">
        <Screen component={HomeScreen} name={screens.HOME} />
        <Screen component={AddQuizStack} name={stacks.ADD_QUIZ} />
        <Screen component={QuizStack} name={screens.QUIZ_LIST} />
    </Navigator>
);

export default HomeStack;
