import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import AuthScreen from "../screens/AuthScreen";
import { screens } from "../screens/enum";
import { stacks } from "./enum";
import HomeStack from "./HomeStack";
import * as firebase from "firebase";
import { config } from "../constants/firestore-config";

export type MainsStackParamsList = {
    [screens.AUTH]: undefined;
    [stacks.HOME]: undefined;
}

const { Navigator, Screen } = createStackNavigator<MainsStackParamsList>();

const MainStack = () => {
    if (!firebase.default.apps.length) firebase.default.initializeApp(config);
    return <Navigator initialRouteName={screens.AUTH} headerMode="none">
        <Screen component={AuthScreen} name={screens.AUTH} />
        <Screen component={HomeStack} name={stacks.HOME} />
    </Navigator>
};

export default MainStack;
