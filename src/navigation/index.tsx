import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import MainStack from "./MainStack";
import MainContext from "../contexts/Context";


const Navigator = () => {
    
  const [text, onChangeText] = React.useState("");
  return(
      <MainContext.Provider value={{text, onChangeText}} >
        <NavigationContainer>
        <MainStack />
        </NavigationContainer>
      </MainContext.Provider>
  )
    
    
};

export default Navigator;
