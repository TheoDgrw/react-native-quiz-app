import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { routes } from "../constants/routes";
import AddQuizName from "../screens/AddQuizNameScreen";
import AddQuizQuestion from "../screens/AddQuizQuestionScreen";
import { screens } from "../screens/enum";
import AddQuizzContext, { AddQuizzProvider } from "../contexts/AddQuizzContext";

export type AddQuizStackParamsList = {
    [screens.ADD_QUIZ_NAME]: undefined;
    [screens.ADD_QUIZ_QUESTION]: undefined;
}

const { Navigator, Screen } = createStackNavigator<AddQuizStackParamsList>();

const AddQuizStack = () =>{

    return(
        <AddQuizzProvider>
            <Navigator initialRouteName={screens.ADD_QUIZ_NAME}>
                <Screen component={AddQuizName} name={screens.ADD_QUIZ_NAME} />
                <Screen component={AddQuizQuestion} name={screens.ADD_QUIZ_QUESTION} />
            </Navigator>
        </AddQuizzProvider>
);
} 

export default AddQuizStack;
