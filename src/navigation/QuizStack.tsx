import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { routes } from "../constants/routes";
import QuizStartScreen from "../screens/QuizListScreen";
import QuizQuestionScreen from "../screens/QuizQuestionScreen";
import QuizResultScreen from "../screens/QuizResultScreen";
import { screens } from "../screens/enum";
import AddQuizzContext from "../contexts/AddQuizzContext";
import IQuiz, { IAnswer } from "../interfaces/IQuiz";

export type QuizStackParamsList = {
    [screens.QUIZ_LIST]: undefined;
    [screens.QUIZ_QUESTION]: { quiz: IQuiz, quizName: string };
    [screens.QUIZ_RESULTS]: { quiz: IQuiz, responses: IAnswer[] };
}

const { Navigator, Screen } = createStackNavigator<QuizStackParamsList>();

const QuizStack = () => {
    return(
        
            <Navigator initialRouteName={screens.QUIZ_LIST}>
                <Screen component={QuizStartScreen} name={screens.QUIZ_LIST} />
                <Screen component={QuizQuestionScreen} name={screens.QUIZ_QUESTION} />
                <Screen component={QuizResultScreen} name={screens.QUIZ_RESULTS} />
            </Navigator>
        
    )
    
}

export default QuizStack;
