import { useEffect, useState } from "react";
import * as firebase from "firebase";
import fb from "firebase";

export default function useApiCB<T>(cb: () => Promise<T>) {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [res, setRes] = useState<T | null>(null);
    const [error, setError] = useState<fb.firestore.FirestoreError | unknown | null>(null);

    useEffect(() => {
        if (firebase.default.apps.length) {
            try {
                cb().then((data) => {
                    setRes(data);
                });
            } catch (e: fb.firestore.FirestoreError | unknown) {
                setError(e);
            } finally {
                setIsLoading(false);
            }
        }
    }, []);

    return {
        isLoading,
        res,
        error,
    }
}