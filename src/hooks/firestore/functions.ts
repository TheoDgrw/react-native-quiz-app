import IQuiz from "../../interfaces/IQuiz";
import * as firebase from "firebase";

export const addQuiz = async (quizName: string, quiz: IQuiz) => {
    const db = firebase.default.firestore();
    return await db.collection('quiz')
        .doc(quizName)
        .set(quiz)
        .then(() => {
            return null;
        });
}

export const getQuiz = async (quizName: string) => {
    const db = firebase.default.firestore();
    const res = await db.collection('quiz')
        .doc(quizName)
        .get();
    if (!res.exists) {
        throw new Error();
    }
    return res;
}

export const getAllQuiz = async () => {
    const db = firebase.default.firestore();
    const res = await db.collection('quiz')
        .get();
    return res;
}