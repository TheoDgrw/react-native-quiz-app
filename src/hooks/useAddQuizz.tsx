import React, { useContext } from 'react'
import { useState } from 'react';
import AddQuizzContext from '../contexts/AddQuizzContext';
import MainContext from '../contexts/Context';
import { IAnswer, IQuestion } from '../interfaces/IQuiz';

export default function useAddQuizz() {
    const { quizzName, questions, onChangeQuestion, onChangeQuizzName, quizz, onChangeQuizz } = useContext(AddQuizzContext);
    const { text } = useContext(MainContext)
    const [ question, setQuestion ] = useState("") 
    const [ answers, setAnswers ] = useState<IAnswer[]>([{content: "", id:1, isCorrect: false},{content: "", id:2, isCorrect: false},{content: "", id:3, isCorrect: false},{content: "", id: 4, isCorrect: false}]) 

    function submitQuestion() {

        if(answers.length === 4)   onChangeQuestion([...questions, {question, answers}])

    }
    return (
        {
            author: text,
            questions,
            quizzName,
            onChangeQuizzName,
            setQuestion,
            question,
            answers,
            setAnswers,
            submitQuestion,
            quizz,
            onChangeQuizz
        }


    )
}
